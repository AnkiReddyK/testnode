#install Docker on linux
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04

#Build image
docker build -t your_dockerhub_username/nodejs-image-demo .

#check images
docker image ls

#Run Docker container
docker run --name nodejs-image-demo -p 80:8080 -d your_dockerhub_username/nodejs-image-demo 
#Check containers 
docker container ls

#Check docker logs 
docker logs <container-id>

#login inside the docker 
docker exec -it <container-id> /bin/bash

#Remove docker image
docker rmi <id>

#Remove container..... 
docker rm <id>
