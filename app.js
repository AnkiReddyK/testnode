const express = require('express');
const app = express();
const router = express.Router();

const path = __dirname + '/views/';
const port = 8080;

router.use(function (req,res,next) {
  console.log('/' + req.method);
  next();
});

router.get('/', function(req,res){
if(process.env.NODE_ENV === 'development') console.log('hello world in development');
if(process.env.NODE_ENV === 'production') console.log('hello world in production');

  res.sendFile(path + 'index.html');
});

router.get('/sharks', function(req,res){
  res.sendFile(path + 'sharks.html');
});

app.use(express.static(path));
app.use('/', router);

app.listen(port, function () {
if(process.env.NODE_ENV === 'development') console.log('hello world from india');
if(process.env.NODE_ENV === 'production') console.log('hello world in production');
  console.log('Example app listening on port 8080!')
})
